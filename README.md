# Java Spring Boot REST Web Service Users and Posts App with Authentication and Authorization

Implemented in Java using Spring Boot libraries. Documentation webpage is in HTML.

Detailed docs of app is in  src/main/resources/templates/apidocs.html.

REST methods are automatically tested using SpringBootTest. The tests are in src/test/java/rznu/controller.

Maven project has dependencies in pom.xml.

Controller methods, which add/delete users/posts, are executed using Postman API platform (https://www.postman.com/). Current list i.e. bases of users and posts are in "users.json" and "posts.json" files respectively.

My project in Service-Oriented Computing, FER, Zagreb.

Task description in "TaskSpecification.pdf".

Created: 2020
