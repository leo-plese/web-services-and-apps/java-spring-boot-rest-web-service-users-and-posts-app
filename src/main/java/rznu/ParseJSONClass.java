package rznu;

import java.io.*;

import java.io.FileWriter;
import java.io.FileReader;

import org.json.simple.parser.JSONParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import rznu.models.PostModel;
import rznu.models.UserModel;

public class ParseJSONClass {

    public static void writeJSONObjectList(JSONArray usersList, String filename) {
        //Write JSON file
        try (FileWriter file = new FileWriter(filename)) {

            file.write(usersList.toJSONString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static void writeUserJSONObject(JSONArray usersList, UserModel userModel, String filename) {
        JSONObject userDetails = new JSONObject();
        userDetails.put("id", userModel.getId());
        userDetails.put("role", userModel.getRole());
        userDetails.put("email", userModel.getEmail());
        userDetails.put("username", userModel.getUsername());
        userDetails.put("password", userModel.getPassword());

        usersList.add(userDetails);

        writeJSONObjectList(usersList, filename);

    }


    public static void writePostJSONObject(JSONArray postsList, PostModel postModel, String filename) {
        JSONObject postDetails = new JSONObject();
        postDetails.put("id", postModel.getId());
        postDetails.put("userId", postModel.getUserId());
        postDetails.put("text", postModel.getText());


        postsList.add(postDetails);

        writeJSONObjectList(postsList, filename);

    }

    public static JSONArray readJSONObjects(String filename) {
        File usersJSONFile = new File(filename);
        JSONArray usersList = new JSONArray();

        try {
            if (!usersJSONFile.exists()) {
                usersJSONFile.createNewFile();

                writeJSONObjectList(usersList, filename);
            } else {

                JSONParser jsonParser = new JSONParser();


                try (FileReader reader = new FileReader(filename))
                {
                    //Read JSON file
                    Object obj = jsonParser.parse(reader);

                    usersList = (JSONArray) obj;
//                    System.out.println(usersList);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }



        return usersList;
    }

}
