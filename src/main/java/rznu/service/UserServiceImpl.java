package rznu.service;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import rznu.ParseJSONClass;
import rznu.models.UserModel;

import java.util.ArrayList;
import java.util.List;


// For a class to be used as a dependency injection (independent) we run it as a @Service.
//public class UserServiceImpl implements UserService {
@Service
public class UserServiceImpl implements UserDetailsService {


    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;


    // used in user Sign-in (login) - authentication
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserModel userModel = null;
        try {
            JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");

            for (Object userObject : usersList) {
                JSONObject userJSONObject = (JSONObject) userObject;

                if ((((JSONObject)userObject).get("username")).equals(username)) {
                    userModel = new UserModel();

                    userModel.setId((Long)userJSONObject.get("id"));
                    userModel.setRole((String)userJSONObject.get("role"));
                    userModel.setEmail((String)userJSONObject.get("email"));
                    userModel.setUsername((String)userJSONObject.get("username"));
                    userModel.setPassword((String)userJSONObject.get("password"));

                    break;
                }
            }
        } catch (Exception ex) {
            throw new UsernameNotFoundException("An exception occurred loadUserByUsername (auth): username = \"" + username + "\"!");
        }

        // if user with given username is not found or is disabled (field "is_enabled" in table "user")
        // prevent him from logging in
        if (userModel == null) {

            throw new UsernameNotFoundException(username);
        }


        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(userModel.getRole()));

        return new User(userModel.getUsername(), userModel.getPassword(), authorities);
    }

}
