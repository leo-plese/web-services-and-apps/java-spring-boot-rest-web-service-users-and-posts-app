package rznu.controller;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import rznu.ParseJSONClass;
import rznu.models.PostModel;
import rznu.models.UserModel;

@RestController
@RequestMapping("/api")
public class PublicController {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


//    @GetMapping(path = "/public")
//    public String getMessage() {
//        return "Hello from public API controller!!!";
//    }


    //////////////


    private static JSONObject getUserWithUsername(JSONArray usersList, String username) {
        for (Object userObject : usersList) {
            JSONObject userJSONObject = (JSONObject) userObject;
            if (((userJSONObject).get("username")).equals(username)) {
                return userJSONObject;
            }
        }

        return null;
    }

    private static boolean doesUserWithIdExist(JSONArray usersList, Long id) {
        for (Object userObject : usersList) {
            if ((((JSONObject) userObject).get("id")).equals(id)) {
                return true;
            }
        }

        return false;
    }

    @PostMapping(path = "/users")
    public UserModel createUser(@RequestBody UserModel userModel) {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");

        String errorNullMsg = "";

        if (userModel.getPassword() == null) {
            errorNullMsg += "Password must be entered! ";
        }

        if (userModel.getUsername() == null) {
            errorNullMsg += "Username must be entered! ";
        }

        if (userModel.getEmail() == null) {
            errorNullMsg += "Email must be entered! ";
        }

        if (!errorNullMsg.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorNullMsg);
        }

        // check if a user with username already exists -> if yes, cannot create new user with same username
        if (getUserWithUsername(usersList, userModel.getUsername()) != null) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "COULD NOT create a user - user with username " + userModel.getUsername() + " already exists!");
            //return "COULD NOT create a user - user with username " + userModel.getUsername() + " already exists!";
        }

        long userId;
        int userListSize = usersList.size();
        if (userListSize == 0) {
            userId = 1;
        } else {
            userId = (Long) ((JSONObject) usersList.get(userListSize - 1)).get("id") + 1;
        }

        userModel.setRole("ROLE_USER");
        userModel.setId(userId);
        userModel.setPassword(bCryptPasswordEncoder.encode(userModel.getPassword()));

        ParseJSONClass.writeUserJSONObject(usersList, userModel, "users.json");

        return userModel;
    }


    @Secured({"ROLE_ADMIN"})
    @DeleteMapping(path = "/users/{id}")
    public void deleteUser(@PathVariable Long id) {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");


        JSONArray usersListCpy = new JSONArray();
        usersListCpy.addAll(usersList);

        for (Object userObject : usersList) {
            if ((((JSONObject) userObject).get("id")).equals(id)) {
                usersListCpy.remove(userObject);

                break;
            }
        }

        usersList = usersListCpy;

        ParseJSONClass.writeJSONObjectList(usersList, "users.json");

        ///////////
        // delete all user's posts
        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");


        JSONArray postsListCpy = new JSONArray();
        postsListCpy.addAll(postsList);
        for (Object postObject : postsList) {
            if ((((JSONObject) postObject).get("userId")).equals(id)) {
                postsListCpy.remove(postObject);
            }
        }

        postsList = postsListCpy;

        ParseJSONClass.writeJSONObjectList(postsList, "posts.json");
    }


    @DeleteMapping(path = "/users")
    public void deleteUserLoggedIn() {
        String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");


        JSONArray usersListCpy = new JSONArray();
        usersListCpy.addAll(usersList);

        boolean foundUserForDel = false;
        Long id = null;
        for (Object userObject : usersList) {
            JSONObject userJSONObject = (JSONObject) userObject;
            if ((userJSONObject.get("username")).equals(username)) {
                id = (Long) userJSONObject.get("id");
                usersListCpy.remove(userObject);

                foundUserForDel = true;
                break;
            }
        }

        if (!foundUserForDel) {
            return;
        }

        usersList = usersListCpy;

        ParseJSONClass.writeJSONObjectList(usersList, "users.json");

        ///////////
        // delete all user's posts
        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");


        JSONArray postsListCpy = new JSONArray();
        postsListCpy.addAll(postsList);
        for (Object postObject : postsList) {
            if ((((JSONObject) postObject).get("userId")).equals(id)) {
                postsListCpy.remove(postObject);
            }
        }

        postsList = postsListCpy;

        ParseJSONClass.writeJSONObjectList(postsList, "posts.json");

    }


    @Secured({"ROLE_ADMIN"})
    @GetMapping(path = "/users/{id}")
    public UserModel getUser(@PathVariable Long id) {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        UserModel userModel = null;

        for (Object userObject : usersList) {
            JSONObject userJSONObject = (JSONObject) userObject;

            if ((((JSONObject) userObject).get("id")).equals(id)) {
                userModel = new UserModel();

                userModel.setId(id);
                userModel.setRole((String) userJSONObject.get("role"));
                userModel.setEmail((String) userJSONObject.get("email"));
                userModel.setUsername((String) userJSONObject.get("username"));
                userModel.setPassword((String) userJSONObject.get("password"));

                break;
            }
        }

        if (userModel == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "COULD NOT get user - user with id " + id + " was not found.");
        }


        return userModel;
    }

    @GetMapping(path = "/users")
    public UserModel getUserLoggedIn() {
        String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");


        JSONArray usersListCpy = new JSONArray();
        usersListCpy.addAll(usersList);

        UserModel userModel = null;
        for (Object userObject : usersList) {
            JSONObject userJSONObject = (JSONObject) userObject;
            if ((userJSONObject.get("username")).equals(username)) {
                userModel = new UserModel();

                userModel.setId((Long) userJSONObject.get("id"));
                userModel.setRole((String) userJSONObject.get("role"));
                userModel.setEmail((String) userJSONObject.get("email"));
                userModel.setUsername((String) userJSONObject.get("username"));
                userModel.setPassword((String) userJSONObject.get("password"));

                break;
            }
        }

        return userModel;
    }


    @PutMapping(path = "/users")
    public UserModel updateUser(@RequestBody UserModel userModel) {
        String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();


        String userEmail = userModel.getEmail();
        String userUsername = userModel.getUsername();
        String userModelPassword = userModel.getPassword();

        if (userEmail == null && userUsername == null && userModelPassword == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Update must update at least one of these 3 fields: email, username, password!");
        }


        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");


        JSONObject userJSONObjectFound = null;
        int indexFound = -1;
        Long userId = null;
        String userRole = null;
        int userListSize = usersList.size();
        for (int i = 0; i < userListSize; i++) {
            Object userObject = usersList.get(i);
            userJSONObjectFound = (JSONObject) userObject;

            if (userJSONObjectFound.get("username").equals(username)) {
                userId = (Long) userJSONObjectFound.get("id");
                userRole = (String) userJSONObjectFound.get("role");
                indexFound = i;
                break;
            }
        }


        // check if a user with username already exists -> if yes, cannot create new user with same username
        JSONObject userWithUsername = getUserWithUsername(usersList, userModel.getUsername());
        if (userWithUsername != null && !(userWithUsername.get("id")).equals(userId)) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "COULD NOT update a user - user with username " + userModel.getUsername() + " already exists!");
        }


        JSONObject userDetails = new JSONObject();

        userDetails.put("id", userId);
        userDetails.put("role", userRole);

        // userEmail == null means user has not entered email to be updated <->  "username" : "XXXX" missing
        if (userEmail != null)
            userDetails.put("email", userEmail);
        else
            userDetails.put("email", userJSONObjectFound.get("email"));

        if (userUsername != null)
            userDetails.put("username", userUsername);
        else
            userDetails.put("username", userJSONObjectFound.get("username"));

        if (userModelPassword != null)
            //userDetails.put("password", userModelPassword);
            userDetails.put("password", bCryptPasswordEncoder.encode(userModel.getPassword()));
        else
            userDetails.put("password", userJSONObjectFound.get("password"));


        usersList.remove(indexFound);
        usersList.add(indexFound, userDetails);


        ParseJSONClass.writeJSONObjectList(usersList, "users.json");

        UserModel userModelNew = new UserModel((String) userDetails.get("email"), (String) userDetails.get("username"), (String) userDetails.get("password"));
        userModelNew.setRole((String) userDetails.get("role"));
        userModelNew.setId((Long) userDetails.get("id"));


        return userModelNew;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////

    @PostMapping(path = "/posts")
    public PostModel createUserPost(@RequestBody PostModel postModel) {
        String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

        if (postModel.getText() == null || postModel.getText().isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Text must be entered!");
        }


        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");

        Long userId = null;
        int userListSize = usersList.size();
        for (int i = 0; i < userListSize; i++) {
            Object userObject = usersList.get(i);
            JSONObject userJSONObject = (JSONObject) userObject;

            if (userJSONObject.get("username").equals(username)) {
                userId = (Long) userJSONObject.get("id");
                break;
            }
        }

        postModel.setUserId(userId);


        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");

        long postId;
        int postListSize = postsList.size();
        if (postListSize == 0) {
            postId = 1;
        } else {
            postId = (Long) ((JSONObject) postsList.get(postListSize - 1)).get("id") + 1;
        }

        postModel.setId(postId);

        ParseJSONClass.writePostJSONObject(postsList, postModel, "posts.json");

        return postModel;
    }

    @DeleteMapping(path = "/posts/{id}")
    public void deletePost(@PathVariable Long id) {
        String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");

        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = null;
        int userListSize = usersList.size();
        for (int i = 0; i < userListSize; i++) {
            Object userObject = usersList.get(i);
            JSONObject userJSONObject = (JSONObject) userObject;

            if (userJSONObject.get("username").equals(username)) {
                userId = (Long) userJSONObject.get("id");
                break;
            }
        }


        JSONArray postsListCpy = new JSONArray();
        postsListCpy.addAll(postsList);
        for (Object postObject : postsList) {
            JSONObject postJSONObject = (JSONObject) postObject;
            if (postJSONObject.get("id").equals(id)) {
                if (!postJSONObject.get("userId").equals(userId)) {
                    throw new ResponseStatusException(HttpStatus.FORBIDDEN, "CANNOT delete post from other user!");
                }

                postsListCpy.remove(postObject);
            }
        }


        postsList = postsListCpy;

        ParseJSONClass.writeJSONObjectList(postsList, "posts.json");
    }

    @GetMapping(path = "/posts/{id}")
    public PostModel getPost(@PathVariable Long id) {
        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");
        PostModel postModel = null;

        for (Object postObject : postsList) {
            JSONObject postJSONObject = (JSONObject) postObject;

            if ((((JSONObject) postObject).get("id")).equals(id)) {
                postModel = new PostModel();

                postModel.setId(id);
                postModel.setUserId((Long) postJSONObject.get("userId"));
                postModel.setText((String) postJSONObject.get("text"));

                break;
            }
        }

        if (postModel == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "COULD NOT get post - post with id " + id + " was not found.");
        }


        return postModel;
    }


    @PutMapping(path = "/posts/{id}")
    public PostModel updatePost(@PathVariable Long id, @RequestBody PostModel postModel) {
        String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

        String text = postModel.getText();

        if (text == null || text.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Update must update field: text!");
        }


        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");

        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = null;
        int userListSize = usersList.size();
        for (int i = 0; i < userListSize; i++) {
            Object userObject = usersList.get(i);
            JSONObject userJSONObject = (JSONObject) userObject;

            if (userJSONObject.get("username").equals(username)) {
                userId = (Long) userJSONObject.get("id");
                break;
            }
        }

        JSONObject postDetails  = null;
        int postListSize = postsList.size();
        for (int i = 0; i < postListSize; i++) {
            Object postObject = postsList.get(i);
            JSONObject postJSONObject = (JSONObject) postObject;

            if (postJSONObject.get("id").equals(id)) {
                if (!postJSONObject.get("userId").equals(userId)) {
                    throw new ResponseStatusException(HttpStatus.FORBIDDEN, "CANNOT update post from other user!");
                }

                postsList.remove(i);

                postDetails = new JSONObject();
                postDetails.put("id", id);


                postDetails.put("text", text);

                postDetails.put("userId", postJSONObject.get("userId"));


                postsList.add(i, postDetails);

                break;
            }
        }

        if (postDetails == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "COULD NOT update post - post with id " + id + " was not found.");
        }

        ParseJSONClass.writeJSONObjectList(postsList, "posts.json");

        PostModel postModelNew = new PostModel((String)postDetails.get("text"));
        postModelNew.setId((Long)postDetails.get("id"));
        postModelNew.setUserId((Long)postDetails.get("userId"));

        return postModelNew;
    }

    //////////////////////////////////////////////
    @GetMapping(path = "/users/{usrid}/posts")
    public JSONArray getUserPosts(@PathVariable Long usrid) {
        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");

        JSONArray userPostsList = new JSONArray();

        boolean userFound = false;
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        int userListSize = usersList.size();
        for (int i = 0; i < userListSize; i++) {
            Object userObject = usersList.get(i);
            JSONObject userJSONObject = (JSONObject) userObject;

            if (userJSONObject.get("id").equals(usrid)) {
                userFound = true;
                break;
            }
        }

        if (!userFound) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "COULD NOT get user's posts - user with id " + usrid + " was not found.");
        }


        for (Object postObject : postsList) {
            JSONObject postJSONObject = (JSONObject) postObject;

            if (postJSONObject.get("userId").equals(usrid)) {
                Long postId = (Long) postJSONObject.get("id");
                String text = (String) postJSONObject.get("text");



                JSONObject postDetails = new JSONObject();
                postDetails.put("id", postId);
                postDetails.put("userId", usrid);
                postDetails.put("text", text);


                userPostsList.add(postDetails);
            }
        }


        return userPostsList;
    }


}
