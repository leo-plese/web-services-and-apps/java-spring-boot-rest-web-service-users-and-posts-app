package rznu.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
public class DocController {

    @GetMapping
    public String getRESTApiDocs() {
        return "apidocs";
    }
}
