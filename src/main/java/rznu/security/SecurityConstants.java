package rznu.security;

import rznu.SpringApplicationContext;
import rznu.AppProperties;

public final class SecurityConstants {

    public static final String AUTH_LOGIN_URL = "/api/authenticate";


    // JWT token defaults
    // some are used in creating token in authentication in JwtAuthenticationFilter
    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_TYPE = "JWT";
    public static final String TOKEN_ISSUER = "secure-api";
    public static final String TOKEN_AUDIENCE = "secure-app";
    public static final int TOKEN_VALIDITY_PERIOD = 86400000;//30000;

    private SecurityConstants() {
        throw new IllegalStateException("Cannot create instance of static util class");
    }

    public static String getTokenSecret() {
        AppProperties appProperties = (AppProperties) SpringApplicationContext.getBean("AppProperties");
        return appProperties.getTokenSecret();
    }
}
