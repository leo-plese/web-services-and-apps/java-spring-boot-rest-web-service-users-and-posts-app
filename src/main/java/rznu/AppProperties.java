package rznu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * Reads application.properties file. For that we need @Component annotation above the class rznu.AppProperties and @Autowired Environment field.
 */

@Component
public class AppProperties {

    @Autowired
    private Environment env;

    public String getTokenSecret() {
        // String input "tokenSecret" is name of the corresponding variable defined in application.properties
        return env.getProperty("tokenSecret");
    }


}
