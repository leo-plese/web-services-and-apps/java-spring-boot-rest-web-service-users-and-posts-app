package rznu.controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import rznu.ParseJSONClass;
import rznu.models.PostModel;
import rznu.models.UserModel;
import rznu.security.SecurityConstants;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
public class PublicControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private Long getLastObjectId(JSONArray objectsList) {
        int objectListSize = objectsList.size();
        Long objectId;
        if (objectListSize  == 0) {
            objectId = 1L;
        } else {
            objectId = (Long) ((JSONObject) objectsList.get(objectListSize  - 1)).get("id") + 1L;
        }
        return objectId;
    }

    private void addUserToUserList(JSONArray usersList, Long userId, Long userNum) {
        String userText = "usertest"+userNum;
        UserModel userModel = new UserModel(userId, userText+"@example.com",userText,
                bCryptPasswordEncoder.encode(userText+"_pwd"), "ROLE_USER");
        ParseJSONClass.writeUserJSONObject(usersList, userModel, "users.json");
    }

    private void addAdminToUserList(JSONArray usersList, Long userId) {
        String userText = "admintest";
        UserModel userModel = new UserModel(userId, userText+"@example.com",userText,
                bCryptPasswordEncoder.encode(userText+"_pwd"), "ROLE_ADMIN");
        ParseJSONClass.writeUserJSONObject(usersList, userModel, "users.json");
    }

    private void removeLastNUsersFromUserList(int nUsersDel) {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        for (int i=0; i<nUsersDel; i++) {
            usersList.remove(usersList.size() - 1);
        }
        ParseJSONClass.writeJSONObjectList(usersList, "users.json");
    }


    private String addPostToPostList(JSONArray postsList, Long postId, Long userId, Long postNo) {
        String postTxt = "userId = "+userId+"; my post no. "+postNo;
        PostModel postModel = new PostModel(postId, userId, postTxt);
        ParseJSONClass.writePostJSONObject(postsList, postModel, "posts.json");

        return postTxt;
    }

    private void removeLastNPostsFromPostList(int nPostsDel) {
        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");
        for (int i=0; i<nPostsDel; i++) {
            postsList.remove(postsList.size() - 1);
        }
        ParseJSONClass.writeJSONObjectList(postsList, "posts.json");
    }

    //////////////////////////
    @Test
    public void loginTestExistingUserOk() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        // test - START
        String body = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        Assertions.assertNotNull(mvcResult.getResponse().getHeader("Authorization"));
        // test - END

        removeLastNUsersFromUserList(1);
    }

    @Test
    public void loginTestNonexistingUserUnauthorized() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        // test - START
        String body = "{\"password\" : \"usertest0_pwdXXX\", \"username\" : \"usertest0\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized()).andReturn();
        Assertions.assertNull(mvcResult.getResponse().getHeader("Authorization"));
        // test - END

        removeLastNUsersFromUserList(1);
    }

    //////////////////
    @Test
    public void createUserExistingUsernameUserConflict() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        // test - START
        String body = "{\"password\" : \"new_user_pwd\", \"username\" : \"usertest0\",\"email\" : \"new_user@example.com\"}";
        mockMvc.perform(post("/api/users").content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isConflict())
                .andExpect(status().reason("COULD NOT create a user - user with username " + "usertest0" + " already exists!"));
        // test - END

        removeLastNUsersFromUserList(1);
    }

    @Test
    public void createUserNonexistingUsernameUserOk() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        userId++;

        // test - START
        String body = "{\"password\" : \"usertest1_pwd\", \"username\" : \"usertest1\",\"email\" : \"usertest1@example.com\"}";
        mockMvc.perform(post("/api/users").content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(userId.intValue())))
                .andExpect(jsonPath("$.email", is("usertest1@example.com")))
                .andExpect(jsonPath("$.username", is("usertest1")))
                .andExpect(jsonPath("$.role", is("ROLE_USER")));
        // test - END

        removeLastNUsersFromUserList(2);
    }

    @Test
    public void createUserBadRequest() throws Exception {
        // test - START
        String body = "{\"password\" : \"new_user_pwd\", \"username\" : \"new_user\"}";
        mockMvc.perform(post("/api/users").content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest()).andExpect(status().reason("Email must be entered! "));
        // test - END
    }

    //////////////////

    @Test
    public void deleteUserRoleUserShouldForbidden() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(delete("/api/users/{id}", userId).header("Authorization", token)).andExpect(status().isForbidden()).andExpect(status().reason("Forbidden"));
        // test - END

        removeLastNUsersFromUserList(1);
    }

    @Test
    public void deleteUserAnonymousShouldAccessDenied() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);


        // test - START
        mockMvc.perform(delete("/api/users/{id}", userId)).andExpect(status().isForbidden()).andExpect(status().reason("Access Denied"));
        // test - END

        removeLastNUsersFromUserList(1);
    }

    @Test
    public void deleteUserRoleAdminShouldOk() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addAdminToUserList(usersList, userId);

        userId++;

        addUserToUserList(usersList, userId, 0L);


        // test - START
        String loginBody = "{\"password\" : \"admintest_pwd\", \"username\" : \"admintest\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(delete("/api/users/{id}", userId).header("Authorization", token)).andExpect(status().isOk());;
        // test - END

        removeLastNUsersFromUserList(1);
    }

    @Test
    public void deleteSameUserRoleAdminShouldOk() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addAdminToUserList(usersList, userId);

        userId++;

        addUserToUserList(usersList, userId, 0L);


        // test - START
        String loginBody = "{\"password\" : \"admin_pwd\", \"username\" : \"admin\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(delete("/api/users/{id}", userId).header("Authorization", token));
        mockMvc.perform(delete("/api/users/{id}", userId).header("Authorization", token)).andExpect(status().isOk());
        // test - END

        removeLastNUsersFromUserList(1);
    }

    //////////////////

    @Test
    public void deleteUserLoggedInAnonymousShouldForbidden() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        // test - START
        mockMvc.perform(delete("/api/users")).andExpect(status().isForbidden()).andExpect(status().reason("Access Denied"));
        // test - END

        removeLastNUsersFromUserList(1);
    }

    @Test
    public void deleteUserLoggedInAuthenticatedShouldOk() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(delete("/api/users").header("Authorization", token)).andExpect(status().isOk());
        // test - END
    }


    //////////////////

    @Test
    public void getUserRoleUserShouldForbidden() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(get("/api/users/{id}", userId).header("Authorization", token)).andExpect(status().isForbidden()).andExpect(status().reason("Forbidden"));
        // test - END

        removeLastNUsersFromUserList(1);
    }

    @Test
    public void getUserAnonymousShouldAccessDenied() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        // test - START
        mockMvc.perform(get("/api/users/{id}", userId)).andExpect(status().isForbidden()).andExpect(status().reason("Access Denied"));
        // test - END

        removeLastNUsersFromUserList(1);
    }

    @Test
    public void getUserRoleAdminShouldOk() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addAdminToUserList(usersList, userId);

        userId++;

        addUserToUserList(usersList, userId, 0L);

        // test - START
        String loginBody = "{\"password\" : \"admintest_pwd\", \"username\" : \"admintest\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(get("/api/users/{id}", userId).header("Authorization", token)).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(userId.intValue())))
                .andExpect(jsonPath("$.email", is("usertest0@example.com")))
                .andExpect(jsonPath("$.username", is("usertest0")))
                .andExpect(jsonPath("$.role", is("ROLE_USER")));
        // test - END

        removeLastNUsersFromUserList(2);
    }

    @Test
    public void getNonexistingUserRoleAdminNotFound() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addAdminToUserList(usersList, userId);

        // test - START
        String loginBody = "{\"password\" : \"admintest_pwd\", \"username\" : \"admintest\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(get("/api/users/{id}", -1).header("Authorization", token)).andExpect(status().isNotFound())
                .andExpect(status().reason("COULD NOT get user - user with id " + -1 + " was not found."));
        // test - END

        removeLastNUsersFromUserList(1);
    }

    //////////////////

    @Test
    public void getUserLoggedInAnonymousShouldForbidden() throws Exception {
        // test - START
        mockMvc.perform(get("/api/users")).andExpect(status().isForbidden()).andExpect(status().reason("Access Denied"));
        // test - END
    }

    @Test
    public void getUserLoggedInAuthenticatedShouldOk() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(get("/api/users").header("Authorization", token)).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(userId.intValue())))
                .andExpect(jsonPath("$.email", is("usertest0@example.com")))
                .andExpect(jsonPath("$.username", is("usertest0")))
                .andExpect(jsonPath("$.role", is("ROLE_USER")));
        // test - END

        removeLastNUsersFromUserList(1);
    }

    //////////////////

    @Test
    public void updateUserAnonymousShouldForbidden() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        // test - START
        String body = "{\"password\" : \"usertest0000_pwd\", \"username\" : \"usertest0000\", \"email\" : \"usertest0000@example.com\"}";

        mockMvc.perform(put("/api/users").content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isForbidden()).andExpect(status().reason("Access Denied"));
        // test - END

        removeLastNUsersFromUserList(1);
    }

    @Test
    public void updateUserEmailPasswordUsernameAuthenticatedShouldOk() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        String body = "{\"password\" : \"usertest0000_pwd\", \"username\" : \"usertest0000\", \"email\" : \"usertest0000@example.com\"}";


        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(put("/api/users").header("Authorization", token).content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(userId.intValue())))
                .andExpect(jsonPath("$.email", is("usertest0000@example.com")))
                .andExpect(jsonPath("$.username", is("usertest0000")))
                .andExpect(jsonPath("$.role", is("ROLE_USER")));
        // test - END

        removeLastNUsersFromUserList(1);
    }

    @Test
    public void updateUserSameEmailPasswordUsernameAuthenticatedShouldOk() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        String body = "{\"password\" : \"usertest0000_pwd\", \"username\" : \"usertest0000\", \"email\" : \"usertest0000@example.com\"}";


        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");
        mockMvc.perform(put("/api/users").header("Authorization", token).content(body).contentType(MediaType.APPLICATION_JSON));

        loginBody = "{\"password\" : \"usertest0000_pwd\", \"username\" : \"usertest0000\"}";
        mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        token = mvcResult.getResponse().getHeader("Authorization");
        mockMvc.perform(put("/api/users").header("Authorization", token).content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(userId.intValue())))
                .andExpect(jsonPath("$.email", is("usertest0000@example.com")))
                .andExpect(jsonPath("$.username", is("usertest0000")))
                .andExpect(jsonPath("$.role", is("ROLE_USER")));
        // test - END

        removeLastNUsersFromUserList(1);
    }

    @Test
    public void updateUserNothingAuthenticatedBadRequest() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        String body = "{}";


        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(put("/api/users").header("Authorization", token).content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
                .andExpect(status().reason("Update must update at least one of these 3 fields: email, username, password!"));
        // test - END

        removeLastNUsersFromUserList(1);
    }

    @Test
    public void updateUserExistingUsernameAuthenticatedConflict() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        userId++;

        addUserToUserList(usersList, userId, 1L);

        // test - START
        String loginBody = "{\"password\" : \"usertest1_pwd\", \"username\" : \"usertest1\"}";

        String body = "{\"username\" : \"usertest0\"}";


        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(put("/api/users").header("Authorization", token).content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isConflict())
                .andExpect(status().reason("COULD NOT update a user - user with username " + "usertest0" + " already exists!"));
        // test - END

        removeLastNUsersFromUserList(2);
    }


    //////////////////

    @Test
    public void createUserPostAnonymousShouldForbidden() throws Exception {

        // test - START
        String body = "{\"text\" : \"my post no. 1\"}";

        mockMvc.perform(post("/api/posts").content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isForbidden()).andExpect(status().reason("Access Denied"));
        // test - END
    }

    @Test
    public void createUserPostAuthenticatedShouldOk() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");
        Long postId = getLastObjectId(postsList);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        String postTxt = "userId = "+userId+"; my post no. 1";
        String body = "{\"text\" : \"" + postTxt + "\"}";


        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(post("/api/posts").header("Authorization", token).content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(postId.intValue())))
                .andExpect(jsonPath("$.userId", is(userId.intValue())))
                .andExpect(jsonPath("$.text", is(postTxt)));
        // test - END


        removeLastNUsersFromUserList(1);
        removeLastNPostsFromPostList(1);
    }


    @Test
    public void createUserPostNoTextFieldAuthenticatedBadRequest() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        String body = "{}";


        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(post("/api/posts").header("Authorization", token).content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
                .andExpect(status().reason("Text must be entered!"));
        // test - END

        removeLastNUsersFromUserList(1);
    }

    @Test
    public void createUserPostEmptyTextFieldAuthenticatedBadRequest() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        String body = "{\"text\" : \"\"}";


        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(post("/api/posts").header("Authorization", token).content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
                .andExpect(status().reason("Text must be entered!"));
        // test - END

        removeLastNUsersFromUserList(1);
    }

    //////////////////

    @Test
    public void deleteUserPostAnonymousShouldForbidden() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");
        Long postId = getLastObjectId(postsList);

        addPostToPostList(postsList, postId, userId, 1L);

        // test - START
        mockMvc.perform(delete("/api/posts/{id}", postId)).andExpect(status().isForbidden()).andExpect(status().reason("Access Denied"));
        // test - END

        removeLastNUsersFromUserList(1);
        removeLastNPostsFromPostList(1);
    }

    @Test
    public void deleteUserPostAuthenticatedShouldOk() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");
        Long postId = getLastObjectId(postsList);

        addPostToPostList(postsList, postId, userId, 1L);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(delete("/api/posts/{id}", postId).header("Authorization", token)).andExpect(status().isOk());
        // test - END

        removeLastNUsersFromUserList(1);
    }

    @Test
    public void deleteSameUserPostAuthenticatedShouldOk() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");
        Long postId = getLastObjectId(postsList);

        addPostToPostList(postsList, postId, userId, 1L);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(delete("/api/posts/{id}", postId).header("Authorization", token));
        mockMvc.perform(delete("/api/posts/{id}", postId).header("Authorization", token)).andExpect(status().isOk());
        // test - END

        removeLastNUsersFromUserList(1);
    }

    @Test
    public void deleteOtherUsersPostAuthenticatedForbidden() throws Exception {
        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");
        Long postId = getLastObjectId(postsList);

        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);

        addUserToUserList(usersList, userId, 0L);
        addPostToPostList(postsList, postId, userId, 1L);

        userId++;
        postId++;

        addUserToUserList(usersList, userId, 1L);
        addPostToPostList(postsList, postId, userId, 1L);


        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(delete("/api/posts/{id}", postId).header("Authorization", token)).andExpect(status().isForbidden())
                .andExpect(status().reason("CANNOT delete post from other user!"));
        // test - END

        removeLastNUsersFromUserList(2);
        removeLastNPostsFromPostList(2);
    }

    //////////////////

    @Test
    public void getPostAnonymousShouldForbidden() throws Exception {
        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");
        Long postId = getLastObjectId(postsList);

        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);

        addUserToUserList(usersList, userId, 0L);
        addPostToPostList(postsList, postId, userId, 1L);

        // test - START
        mockMvc.perform(get("/api/posts/{id}", postId)).andExpect(status().isForbidden()).andExpect(status().reason("Access Denied"));
        // test - END

        removeLastNUsersFromUserList(1);
        removeLastNPostsFromPostList(1);
    }

    @Test
    public void getPostAuthenticatedShouldOk() throws Exception {
        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");
        Long postId = getLastObjectId(postsList);

        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);

        addUserToUserList(usersList, userId, 0L);
        String postTxt = addPostToPostList(postsList, postId, userId, 1L);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(get("/api/posts/{id}", postId).header("Authorization", token)).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(postId.intValue())))
                .andExpect(jsonPath("$.userId", is(userId.intValue())))
                .andExpect(jsonPath("$.text", is(postTxt)));
        // test - END

        removeLastNUsersFromUserList(1);
        removeLastNPostsFromPostList(1);
    }

    @Test
    public void getNonexistingPostNotFound() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(get("/api/posts/{id}", -1).header("Authorization", token)).andExpect(status().isNotFound())
                .andExpect(status().reason("COULD NOT get post - post with id " + -1 + " was not found."));
        // test - END

        removeLastNUsersFromUserList(1);
    }

    //////////////////

    @Test
    public void updatePostAnonymousShouldForbidden() throws Exception {
        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");
        Long postId = getLastObjectId(postsList);

        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);

        addUserToUserList(usersList, userId, 0L);
        String postTxt = addPostToPostList(postsList, postId, userId, 1L);

        // test - START
        String body = "{\"text\" : \"" + postTxt + " UPDATED\"}";

        mockMvc.perform(put("/api/posts/{id}", postId).content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isForbidden()).andExpect(status().reason("Access Denied"));
        // test - END

        removeLastNUsersFromUserList(1);
        removeLastNPostsFromPostList(1);
    }

    @Test
    public void updatePostAuthenticatedShouldOk() throws Exception {
        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");
        Long postId = getLastObjectId(postsList);

        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);

        addUserToUserList(usersList, userId, 0L);
        String postTxt = addPostToPostList(postsList, postId, userId, 1L);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        String postNewTxt = postTxt + " UPDATED";
        String body = "{\"text\" : \"" + postNewTxt + "\"}";


        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(put("/api/posts/{id}", postId).header("Authorization", token).content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(postId.intValue())))
                .andExpect(jsonPath("$.userId", is(userId.intValue())))
                .andExpect(jsonPath("$.text", is(postNewTxt)));
        // test - END

        removeLastNUsersFromUserList(1);
        removeLastNPostsFromPostList(1);
    }

    @Test
    public void updatePostSameTextAuthenticatedShouldOk() throws Exception {
        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");
        Long postId = getLastObjectId(postsList);

        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);

        addUserToUserList(usersList, userId, 0L);
        String postTxt = addPostToPostList(postsList, postId, userId, 1L);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        String postNewTxt = postTxt + " UPDATED";
        String body = "{\"text\" : \"" + postNewTxt + "\"}";


        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(put("/api/posts/{id}", postId).header("Authorization", token).content(body).contentType(MediaType.APPLICATION_JSON));
        mockMvc.perform(put("/api/posts/{id}", postId).header("Authorization", token).content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(postId.intValue())))
                .andExpect(jsonPath("$.userId", is(userId.intValue())))
                .andExpect(jsonPath("$.text", is(postNewTxt)));
        // test - END

        removeLastNUsersFromUserList(1);
        removeLastNPostsFromPostList(1);
    }

    @Test
    public void updatePostNoTextAuthenticatedBadRequest() throws Exception {
        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");
        Long postId = getLastObjectId(postsList);

        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);

        addUserToUserList(usersList, userId, 0L);
        String postTxt = addPostToPostList(postsList, postId, userId, 1L);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        String body = "{}";


        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(put("/api/posts/{id}", postId).header("Authorization", token).content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
                .andExpect(status().reason("Update must update field: text!"));
        // test - END

        removeLastNUsersFromUserList(1);
        removeLastNPostsFromPostList(1);
    }

    @Test
    public void updatePostEmptyTextAuthenticatedBadRequest() throws Exception {
        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");
        Long postId = getLastObjectId(postsList);

        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);

        addUserToUserList(usersList, userId, 0L);
        String postTxt = addPostToPostList(postsList, postId, userId, 1L);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        String body = "{\"text\" : \"\"}";


        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(put("/api/posts/{id}", postId).header("Authorization", token).content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
                .andExpect(status().reason("Update must update field: text!"));
        // test - END

        removeLastNUsersFromUserList(1);
        removeLastNPostsFromPostList(1);
    }

    @Test
    public void updateOtherUsersPostAuthenticatedForbidden() throws Exception {
        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");
        Long postId = getLastObjectId(postsList);

        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);

        addUserToUserList(usersList, userId, 0L);
        String postTxt = addPostToPostList(postsList, postId, userId, 1L);

        userId++;
        postId++;

        addUserToUserList(usersList, userId, 1L);
        postTxt = addPostToPostList(postsList, postId, userId, 1L);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        String postNewTxt = "userId = "+userId+"; my post no. 2";
        String body = "{\"text\" : \"" + postNewTxt + "\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(put("/api/posts/{id}", postId).header("Authorization", token).content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isForbidden())
                .andExpect(status().reason("CANNOT update post from other user!"));
        // test - END

        removeLastNUsersFromUserList(2);
        removeLastNPostsFromPostList(2);
    }

    @Test
    public void updateNonexistingPostNotFound() throws Exception {
        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);
        addUserToUserList(usersList, userId, 0L);

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        String body = "{\"text\" : \"userId = " +userId+ "; my post no. 3\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(put("/api/posts/{id}", -1).header("Authorization", token).content(body).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound())
                .andExpect(status().reason("COULD NOT update post - post with id " + -1 + " was not found."));
        // test - END

        removeLastNUsersFromUserList(1);
    }

    //////////////////
    private class UserIdsPostIdsPostTxts {
        private Long[] userIds;
        private Long[] postIds;
        private String[] postTxts;

        public UserIdsPostIdsPostTxts(Long[] userIds, Long[] postIds, String[] postTxts) {
            this.userIds = userIds;
            this.postIds = postIds;
            this.postTxts = postTxts;
        }
    }

    private UserIdsPostIdsPostTxts createFewUsersAndPosts() {
        JSONArray postsList = ParseJSONClass.readJSONObjects("posts.json");
        Long postId = getLastObjectId(postsList);

        JSONArray usersList = ParseJSONClass.readJSONObjects("users.json");
        Long userId = getLastObjectId(usersList);

        Long userId1 = userId, postId1 = postId;
        // usertest0
        addUserToUserList(usersList, userId, 0L);
        // usertest0 - post no 1
        String postTxtUser0Post1 = addPostToPostList(postsList, postId, userId, 1L);

        userId++;
        postId++;

        Long userId2 = userId, postId2 = postId;
        // usertest1
        addUserToUserList(usersList, userId, 1L);
        // usertest1 - post no 1
        String postTxtUser1Post1 = addPostToPostList(postsList, postId, userId, 1L);

        postId++;

        Long postId3 = postId;
        // usertest0 - post no 2
        String postTxtUser0Post2 = addPostToPostList(postsList, postId, userId1, 2L);

        userId++;

        Long userId3 = userId;
        // usertest2
        addUserToUserList(usersList, userId, 2L);

        return new UserIdsPostIdsPostTxts(new Long[]{userId1, userId2, userId3}, new Long[]{postId1, postId2, postId3},
                new String[]{postTxtUser0Post1, postTxtUser1Post1, postTxtUser0Post2});
    }

    @Test
    public void getUserPostsAnonymousShouldForbidden() throws Exception {
        UserIdsPostIdsPostTxts userIdsPostIdsPostTxts = createFewUsersAndPosts();

        // test - START
        mockMvc.perform(get("/api/users/{usrid}/posts", userIdsPostIdsPostTxts.userIds[0])).andExpect(status().isForbidden()).andExpect(status().reason("Access Denied"));
        // test - END

        removeLastNUsersFromUserList(3);
        removeLastNPostsFromPostList(3);
    }

    @Test
    public void getUserPostsAuthenticatedShouldOk() throws Exception {
        UserIdsPostIdsPostTxts userIdsPostIdsPostTxts = createFewUsersAndPosts();
        Long[] userIds = userIdsPostIdsPostTxts.userIds;
        Long[] postIds = userIdsPostIdsPostTxts.postIds;
        String[] postTxts = userIdsPostIdsPostTxts.postTxts;

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(get("/api/users/{usrid}/posts", userIds[0]).header("Authorization", token)).andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(postIds[0].intValue())))
                .andExpect(jsonPath("$[0].userId", is(userIds[0].intValue())))
                .andExpect(jsonPath("$[0].text", is(postTxts[0])))
                .andExpect(jsonPath("$[1].id", is(postIds[2].intValue())))
                .andExpect(jsonPath("$[1].userId", is(userIds[0].intValue())))
                .andExpect(jsonPath("$[1].text", is(postTxts[2])));
        // test - END

        removeLastNUsersFromUserList(3);
        removeLastNPostsFromPostList(3);
    }

    @Test
    public void getUserPostsAuthenticatedShouldOk2() throws Exception {
        UserIdsPostIdsPostTxts userIdsPostIdsPostTxts = createFewUsersAndPosts();
        Long[] userIds = userIdsPostIdsPostTxts.userIds;
        Long[] postIds = userIdsPostIdsPostTxts.postIds;
        String[] postTxts = userIdsPostIdsPostTxts.postTxts;

        // test - START
        String loginBody = "{\"password\" : \"usertest1_pwd\", \"username\" : \"usertest1\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(get("/api/users/{usrid}/posts", userIds[1]).header("Authorization", token)).andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(postIds[1].intValue())))
                .andExpect(jsonPath("$[0].userId", is(userIds[1].intValue())))
                .andExpect(jsonPath("$[0].text", is(postTxts[1])));
        // test - END

        removeLastNUsersFromUserList(3);
        removeLastNPostsFromPostList(3);
    }

    @Test
    public void getUserPostsAuthenticatedShouldOk3() throws Exception {
        UserIdsPostIdsPostTxts userIdsPostIdsPostTxts = createFewUsersAndPosts();
        Long[] userIds = userIdsPostIdsPostTxts.userIds;
        Long[] postIds = userIdsPostIdsPostTxts.postIds;
        String[] postTxts = userIdsPostIdsPostTxts.postTxts;

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        MvcResult mvcResult1 = mockMvc.perform(get("/api/users/{usrid}/posts", userIds[2]).header("Authorization", token)).andExpect(status().isOk()).andReturn();
        Assertions.assertEquals(mvcResult1.getResponse().getContentAsString(), "[]");
        // test - END

        removeLastNUsersFromUserList(3);
        removeLastNPostsFromPostList(3);
    }

    @Test
    public void getNonexistingUsersPostsNotFound() throws Exception {
        UserIdsPostIdsPostTxts userIdsPostIdsPostTxts = createFewUsersAndPosts();

        // test - START
        String loginBody = "{\"password\" : \"usertest0_pwd\", \"username\" : \"usertest0\"}";

        MvcResult mvcResult = mockMvc.perform(post(SecurityConstants.AUTH_LOGIN_URL).content(loginBody).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String token = mvcResult.getResponse().getHeader("Authorization");

        mockMvc.perform(get("/api/users/{usrid}/posts", -1).header("Authorization", token)).andExpect(status().isNotFound())
                .andExpect(status().reason("COULD NOT get user's posts - user with id " + -1 + " was not found."));
        // test - END

        removeLastNUsersFromUserList(3);
        removeLastNPostsFromPostList(3);
    }


}